package com.lz.utils;

import java.util.UUID;

/**
 * @author Zj
 * @date 2018/7/8 0008
 */
public class UUIDUtils {

    public static String getUnique() {
        return UUID.randomUUID().toString().toLowerCase().replaceAll("-", "").substring(0, 20);
    }

}
