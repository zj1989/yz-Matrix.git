package com.lz.service;


import com.lz.model.document.BaseDoc;
import com.lz.model.dto.PageInfo;
import com.lz.model.vo.DocumentCondition;

/**
 * @author 周俭
 */
public interface MongoService<T extends BaseDoc> {
    void save(T t);

    void saveAll(Iterable<T> v);

    PageInfo page(DocumentCondition documentCondition);

    void update(DocumentCondition documentCondition);
}
