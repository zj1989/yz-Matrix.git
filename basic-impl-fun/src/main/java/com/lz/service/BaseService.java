package com.lz.service;


import com.lz.model.dto.Result;
import com.lz.model.entity.CommonEntity;
import com.lz.model.vo.CommonBaseCondition;

import java.util.List;

/**
 * @author 周俭
 */
public interface BaseService<T extends CommonEntity, C extends CommonBaseCondition> {
    Result queryPage(C c);

    Result<T> queryOne(C c);

    Result updateOne(T t);

    Result add(T t);

    Result del(C c);

    Result<List<T>> queryList(C c);
}
