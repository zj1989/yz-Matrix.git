package com.lz.controller;


import com.lz.model.annotations.UserInfoRequired;
import com.lz.model.document.BaseDoc;
import com.lz.model.dto.Result;
import com.lz.model.vo.DocumentCondition;
import com.lz.service.MongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author 周俭
 */
public class BaseMongoController<T extends BaseDoc, C extends DocumentCondition> {

    @Autowired
    private MongoService<T> mongoService;

    @PostMapping("query")
    @UserInfoRequired
    public Result execute(@RequestBody C documentCondition) {
        return Result.success(mongoService.page(documentCondition));
    }

    @PostMapping("update")
    @UserInfoRequired(true)
    public Result update(C documentCondition) {
        documentCondition.updateValid();
        mongoService.update(documentCondition);
        return Result.success(true);
    }
}
