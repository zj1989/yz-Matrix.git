package com.lz.controller;


import com.lz.model.annotations.UserInfoRequired;
import com.lz.model.dto.PageInfo;
import com.lz.model.dto.Result;
import com.lz.model.entity.CommonEntity;
import com.lz.model.vo.CommonBaseCondition;
import com.lz.service.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author 周俭
 */
public class BaseController<T extends CommonEntity, S extends BaseService, C extends CommonBaseCondition> {

    @Autowired
    private S s;

    @PostMapping("page")
    public Result<PageInfo<T>> queryPage(@RequestBody C c) {
        return s.queryPage(c);
    }

    @PostMapping("list")
    public Result<List<T>> queryListAll(@RequestBody C c) {
        return s.queryList(c);
    }

    @PostMapping("detail")
    @UserInfoRequired
    public Result<T> queryOne(@RequestBody C c) {
        return s.queryOne(c);
    }

    @UserInfoRequired(required = true)
    @PostMapping("update")
    public Result update(@RequestBody T t) {
        t.paramsExam();
        return s.updateOne(t);
    }

    @UserInfoRequired(required = true)
    @PostMapping("add")
    public Result add(@RequestBody T t) {
        t.paramsExam();
        return s.add(t);
    }

    @UserInfoRequired(required = true)
    @PostMapping("del")
    public Result del(@RequestBody C c) {
        return s.del(c);
    }

    @UserInfoRequired(required = true)
    @PostMapping("batch/del")
    public Result batchDel(@RequestBody C c) {
        String uniques = c.getUnique();
        if (uniques == null) {
            return Result.fail("unique must not be null.");
        }
        String[] str = uniques.split("[,，；。：:]");
        for (String st : str) {
            c.setUnique(st);
            s.del(c);
        }
        return Result.success(true);
    }
}
