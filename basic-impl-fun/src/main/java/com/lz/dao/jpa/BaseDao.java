package com.lz.dao.jpa;

import com.lz.model.entity.CommonEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.MappedSuperclass;

/**
 * @author 周俭
 */
public interface BaseDao<T extends CommonEntity> extends JpaRepository<T, Long> {
}
