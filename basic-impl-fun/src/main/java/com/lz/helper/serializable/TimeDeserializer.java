package com.lz.helper.serializable;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.SneakyThrows;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 周俭
 */
public class TimeDeserializer extends JsonDeserializer<Date> {
    private static final SimpleDateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    public TimeDeserializer() {
    }

    @SneakyThrows
    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        String date = jsonParser.getText();
        return FORMAT.parse(date);
    }
}
