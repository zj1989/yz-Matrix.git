package com.lz.conf;

/**
 * @author 周俭
 */
public class ThreadLocalUserId {
    public static ThreadLocal<Long> CURRENT_USER_ID = new ThreadLocal<>();
}
