package com.lz.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Component
@ComponentScan("com.lz")
public class InitScan {
}
