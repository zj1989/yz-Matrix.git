package com.lz.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * @author 周俭
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class PageInfo<T> implements Serializable {
    private List<T> content;
    private Integer totalPages;
    private Long totalSize;
    private Integer pageNum;
}
