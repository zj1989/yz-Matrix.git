package com.lz.model.dto;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

public class Result<T> implements Serializable {
    private Boolean isSuccess = false;
    private String msg = "";
    private T body;
    private int resultCode;

    public Result() {
        this.resultCode = 0;
    }

    public Boolean getIsSuccess() {
        return this.isSuccess;
    }

    public Result<T> setIsSuccess(Boolean isSuccess) {
        this.isSuccess = isSuccess;
        return this;
    }

    public String getMsg() {
        return this.msg;
    }

    public Result<T> setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public T getBody() {
        return this.body;
    }

    public Result<T> setBody(T body) {
        this.body = body;
        return this;
    }

    public Integer getResultCode() {
        return this.resultCode;
    }

    public Result<T> setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
        return this;
    }

    public static <S> Result<S> success(S body) {
        return (new Result()).setIsSuccess(true).setMsg("SUCCESS").setResultCode(0).setBody(body);
    }

    public static <S> Result<S> fail(String msg) {
        return new Result().setIsSuccess(false).setMsg(msg).setResultCode(-1);
    }

    @Override
    public String toString() {
        return JSONObject.toJSONString(this);
    }
}
