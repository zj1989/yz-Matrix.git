package com.lz.model.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.lz.helper.serializable.DateTimeSerializer;
import com.lz.utils.UUIDUtils;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Administrator
 * @date 2019/1/21 0021
 */
@Data
@JsonInclude(content = JsonInclude.Include.NON_NULL)
@MappedSuperclass
public class CommonEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    /**
     * 状态，1：正常；2：异常；3：删除；
     */
    @Column(name = "c_status", columnDefinition = "int(3) not null default 1 comment '公共状态，1：正常；2：异常；3：删除；'")
    private Integer status;
    @Column(name = "create_user_id", columnDefinition = "bigint(12) not null default -1 comment '内容创建人id'")
    private Long createUserId;
    @Column(name = "update_user_id", columnDefinition = "bigint(12) not null default -1 comment '内容更新人id'")
    private Long updateUserId;
    @Column(name = "create_time", columnDefinition = "datetime DEFAULT CURRENT_TIMESTAMP")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeSerializer.class)
    private Date createTime;
    @Column(name = "update_time", columnDefinition = "datetime DEFAULT CURRENT_TIMESTAMP")
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonSerialize(using = DateTimeSerializer.class)
    private Date updateTime;
    /**
     * 信息记录唯一码
     */
    @Column(name = "u_unique", length = 40, columnDefinition = "varchar(40) default null comment '内容唯一编码'")
    private String unique;

    public void createInit() {
        if (this.status == null) {
            this.status = 1;
        }
        if (this.createUserId == null) {
            this.createUserId = -1L;
        }
        if (this.updateUserId == null) {
            this.updateUserId = -1L;
        }
        this.createTime = new Date();
        this.updateTime = new Date();
        this.unique = UUIDUtils.getUnique();
    }

    public void paramsExam(){
        return;
    }
}
