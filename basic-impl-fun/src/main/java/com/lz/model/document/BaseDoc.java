package com.lz.model.document;

import lombok.Data;

import java.util.Date;

/**
 * @author 周俭
 */
@Data
public abstract class BaseDoc {
    private String status;
    private Date createTime;
    public abstract String getTitleDesc();
    public abstract Integer getIntType();
    public abstract String getUnique();
}
