package com.lz.model.vo;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.lz.helper.serializable.TimeDeserializer;
import com.lz.model.document.BaseDoc;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 周俭
 */
@Data
@Accessors(chain = true)
public abstract class DocumentCondition<T extends BaseDoc> {
    /**
     * table name: class name
     */
    private String name;
    /**
     * the require column
     * 逗号间隔，需要查询全部则为空
     */
    private String queryColumns;
    /**
     * the condition of query
     */
    private Map<String, Object> eqParams = new HashMap<>();
    private Map<String, String> lkParams = new HashMap<>();
    private Map<String, Object> inParams = new HashMap<>();
    private Map<String, Integer> sortParams = new HashMap<>();

    /**
     * update info
     */
    private Map<String, Object> updateParams = new HashMap<>();
    /**
     * yyyyMMdd
     */
    @JsonDeserialize(using = TimeDeserializer.class)
    private Date startTime;
    @JsonDeserialize(using = TimeDeserializer.class)
    private Date endTime;
    /**
     * start from 0
     */
    private Integer pageNum = 0;
    private Integer pageSize = 20;


    /**
     * get the  class of query object
     */
    public abstract Class<T> getDoc();

    public void updateValid() {
        Assert.notEmpty(this.updateParams, "需要修改的参数不能为空");
        Assert.notEmpty(this.eqParams, "需要查询的参数不能为空");
    }

    public void detailValid() {
        Assert.notEmpty(this.eqParams, "需要查询的参数不能为空");
    }

}
