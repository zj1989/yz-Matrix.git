package com.lz.model.vo;

import com.lz.model.annotations.JpaFields;
import lombok.Data;

import java.io.Serializable;

/**
 * @author Administrator
 * @date 2019/3/15 0015
 */
@Data
public abstract class CommonBaseCondition<T> implements Serializable {
    /**
     * 页数，从1开始
     */
    private Integer pageNum;
    /**
     * 行数，默认20
     */
    private Integer pageSize;
    private int start;
    private String imei;
    /**
     * 创建用户id
     */
    private Long createUserId;
    /**
     * 唯一码，查唯一数据
     */
    @JpaFields(attrType = JpaFields.AttrType.EQ)
    private String unique;
    /**
     * 查询状态，默认为有效状态的查询
     */
    private Integer status;

    private String orderByColumn;
    private String orderDsecByColumn;

    /**
     * 创建查询对象实例
     *
     * @return
     */
    public abstract T getInstance();

    /**
     * 判断查询条件是否为空
     *
     * @return
     */
    public abstract boolean isEmpty();

    public Integer getPageNum() {
        if (pageNum == null || pageNum <= 1) {
            pageNum = 1;
        }
        return pageNum - 1;
    }

    public Integer getStart() {
        return getPageNum() * getPageSize();
    }

    public Integer getPageSize() {
        if (pageSize == null || pageSize <= 0) {
            return 20;
        } else if (pageSize > 200) {
            return 200;
        } else {
            return pageSize;
        }
    }

    public void validator() {
        return;
    }
}
