package com.lz.model.enums;

/**
 * 状态枚举总汇
 * 通用状态、订单状态等等
 *
 * @author 周俭
 */
public enum StatusEnum {
    /* common status */
    STATUS_VALID(1, "有效"),
    STATUS_INVALID(2, "无效"),
    STATUS_DEL(3, "已删除"),
    STATUS_HIDDEN(4, "隐藏");

    public Integer value;
    public String description;

    StatusEnum(Integer value, String description) {
        this.value = value;
        this.description = description;
    }
}
