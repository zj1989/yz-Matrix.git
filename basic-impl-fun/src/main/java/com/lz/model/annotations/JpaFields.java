package com.lz.model.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Administrator
 * @date 2019/3/13 0013
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface JpaFields {
    /**
     * query with field
     *
     * @return
     */
    String fieldName() default "";

    /**
     * weather ignore case,default true
     *
     * @return
     */
    boolean ignoreCase() default true;

    /**
     * weather ignore this property
     * @return
     */
    boolean ignore() default false;

    /**
     * query type
     *
     * @return
     */
    AttrType attrType();

    enum AttrType {
        LK, STW, EDW, EQ;
    }
}
