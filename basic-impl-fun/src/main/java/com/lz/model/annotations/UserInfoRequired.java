package com.lz.model.annotations;

import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author Administrator
 * @date 2019/4/6 0006
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserInfoRequired {
    @AliasFor("required")
    boolean value() default false;
    @AliasFor("value")
    boolean required() default false;
}
