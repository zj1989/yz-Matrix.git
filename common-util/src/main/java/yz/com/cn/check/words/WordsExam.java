package yz.com.cn.check.words;

import lombok.Data;

import java.util.List;

/**
 * @author zj
 */
public class WordsExam {
    public static boolean exam() {
        return false;
    }

    @Data
    public static class ExamResult {
        private String originalSentence;
        private String targetSentence;
        private boolean containsSensitive;
        private List<String> sensitiveWords;
    }
}
