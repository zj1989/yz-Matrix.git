package yz.com.cn.check.words;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zj
 */
public class WordsLoad {

    private static Map<String, WordsInfo> map = new HashMap<>();

    public static Map<String, WordsInfo> getMap() {
        return map;
    }

    public static void addParagraph(String words) {
        if (words == null || words.length() <= 0) {
            return;
        }
        addWords(words.split("[,，.。;；\\n\\t]"));
    }

    public static void addWords(List<String> words) {
        String[] arr = new String[words.size()];
        addWords(words.toArray(arr));
    }

    /**
     * 添加敏感词
     *
     * @param words
     */
    public static void addWords(String... words) {
        if (words == null || words.length <= 0) {
            return;
        }
        String[] strs;
        WordsInfo wordsInfo;
        for (String wd : words) {
            strs = wd.split("");
            WordsInfo preInfo = null;
            for (int i = 0; i < strs.length; i++) {
                String st = strs[i];
                boolean flag = false;
                if (i == strs.length - 1) {
                    flag = true;
                }
                wordsInfo = map.get(st);
                if (wordsInfo == null) {
                    wordsInfo = new WordsInfo(st, flag);
                    map.put(st, wordsInfo);
                }
                wordsInfo.setSensitive(flag || wordsInfo.isSensitive());
                if (flag) {
                    wordsInfo.addWords(wd);
                }
                if (preInfo != null) {
                    preInfo.addWords(wordsInfo);
                }
                preInfo = wordsInfo;
            }
        }
    }

    public static void addFileWords(String filePath, boolean classPath) {
        // TODO 通过文件加载敏感词
    }

    public static void addUrlWords(String url) {
        // TODO 通过远程请求加载敏感词
    }
}
