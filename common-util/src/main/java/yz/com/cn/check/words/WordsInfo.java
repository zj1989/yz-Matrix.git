package yz.com.cn.check.words;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

/**
 * @author zj
 */
@Data
@Accessors(chain = true)
public class WordsInfo {
    private String word;
    /**
     * 是否敏感，默认否
     */
    private boolean sensitive;
    /**
     * 以此字结尾的敏感词
     */
    private List<WordsInfo> words = new ArrayList<>();
    /**
     * 敏感词单词集合
     */
    private List<String> wordsList = new ArrayList<>();

    public WordsInfo(String word, boolean sensitive) {
        this.word = word;
        this.sensitive = sensitive;
    }

    /**
     * 添加下级敏感字
     *
     * @param wordsInfo
     * @return
     */
    public WordsInfo addWords(WordsInfo wordsInfo) {
        int i = this.words.indexOf(wordsInfo);
        if (i >= 0) {
            return words.get(i);
        } else {
            words.add(wordsInfo);
            return wordsInfo;
        }
    }

    /**
     * 敏感词集合
     *
     * @param words
     */
    public void addWords(String words) {
        this.wordsList.add(words);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WordsInfo wordsInfo = (WordsInfo) o;
        return word != null ? word.equals(wordsInfo.word) : wordsInfo.word == null;
    }

    @Override
    public int hashCode() {
        return word != null ? word.hashCode() : 0;
    }
}
