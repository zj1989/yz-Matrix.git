package yz.com.cn.transfer.xml;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author zhoujian
 */
public class XmlContainer {
    /**
     * 节点名称
     */
    private String nodeName;
    private Object nodeValue;
    private Map<String, Object> attributes;
    private List<XmlContainer> children;
    private String timeType = "yyyyMMdd";

    /**
     * 创建节点
     *
     * @param nodeName
     */
    public XmlContainer(String nodeName) {
        this.nodeName = nodeName;
    }

    /**
     * 创建节点
     *
     * @param nodeName
     */
    public XmlContainer(String nodeName, Object nodeValue) {
        this.nodeName = nodeName;
        this.nodeValue = nodeValue;
    }

    /**
     * 添加节点属性
     *
     * @param attrName
     * @param attrValue
     * @return
     */
    public XmlContainer addAttr(String attrName, Object attrValue) {
        if (this.attributes == null) {
            this.attributes = new HashMap<String, Object>();
        }
        this.attributes.put(attrName, attrValue);
        return this;
    }

    /**
     * 添加子节点
     *
     * @param xmlContainer
     * @return
     */
    public XmlContainer addChild(XmlContainer xmlContainer) {
        if (this.children == null) {
            this.children = new ArrayList<XmlContainer>();
        }
        this.children.add(xmlContainer);
        return this;
    }

    /**
     * 添加子节点
     *
     * @param list
     * @return
     */
    public XmlContainer addChildren(List<XmlContainer> list) {
        if (this.children == null) {
            this.children = new ArrayList<XmlContainer>();
        }
        this.children.addAll(list);
        return this;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public Object getNodeValue() {
        return nodeValue;
    }

    public void setNodeValue(Object nodeValue) {
        this.nodeValue = nodeValue;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    public List<XmlContainer> getChildren() {
        return children;
    }

    public void setChildren(List<XmlContainer> children) {
        this.children = children;
    }

    public String getTimeType() {
        return timeType;
    }

    public void setTimeType(String timeType) {
        this.timeType = timeType;
    }
}
