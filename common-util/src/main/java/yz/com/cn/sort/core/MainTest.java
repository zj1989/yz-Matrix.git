package yz.com.cn.sort.core;

/**
 * 主测试类
 *
 * @author zhoujian
 */
public class MainTest {

    public static void main(String[] args) {
        Integer[] arr = new Integer[]{88, 32, 11, 33, 64, 1, 8, 2, 10};
        MainTest.bubblingSort(arr.clone());
        MainTest.quickSort(arr.clone());
    }

    private static void quickSort(Integer[] arr) {
        QuickSort quickSort = new QuickSort();
        quickSort.setArr(arr);
        printArr(quickSort.getArr(), "");
        quickSort.sort();
        printArr(quickSort.getArr(), "快速排序结果：");
    }

    /**
     * 冒泡排序
     */
    private static void bubblingSort(Integer[] arr) {
        BubblingSort bubblingSort = new BubblingSort();
        bubblingSort.setArr(arr);
        printArr(bubblingSort.getArr(), "");
        bubblingSort.sort();
        printArr(bubblingSort.getArr(), "冒泡排序结果：");
    }

    public static void printArr(Comparable[] arr, String name) {
        System.out.println(name);
        for (Comparable ar : arr) {
            System.out.print(ar + " ");
        }
        System.out.println();
    }
}
