public class ClearAfterTest {
    public static void main(String[] args) {
        Runtime.getRuntime().traceMethodCalls(true);
        Runtime.getRuntime().traceInstructions(true);
        System.out.println(buildId(212123));
        System.out.println(Runtime.getRuntime().availableProcessors());
        System.out.println(Runtime.getRuntime().freeMemory());
        System.out.println(Runtime.getRuntime().totalMemory());
        System.out.println(BinaryUtil.toBinary(15));
    }

    public static Long buildId(Integer index) {
        Long orderId = System.currentTimeMillis();
        //追加3位序号(不足补零),防止并发时，号码重复
        String idx = String.format("%05d", index);
        if (idx.length() > 5) {
            idx = idx.substring(idx.length() - 5);
        }
        return Long.parseLong(orderId.toString().concat(idx));
    }
}
