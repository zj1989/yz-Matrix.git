import java.util.Arrays;

public class N次方算法 {
    public static String power(Integer orginal, Integer power) {
        int[] result = new int[]{orginal};
        for (int i = 1; i < power; i++) {
            result = method(result, orginal);
        }
        StringBuilder sbt = new StringBuilder();
        for (int i : result) {
            sbt.append(i);
        }
        return sbt.toString();
    }

    private static int[] method(int[] s, Integer orginal) {
        int[] rss = new int[s.length + 1];
        System.arraycopy(s, 0, rss, 1, s.length);
        int x = 0;
        for (int i = rss.length - 1; i >= 0; i--) {
            int pl = rss[i] * orginal + x;
            rss[i] = pl % 10;
            x = pl / 10;
        }
        if (rss[0] == 0) {
            return Arrays.copyOfRange(rss, 1, rss.length);
        }
        return rss;
    }

    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        System.out.println(power(2, 1000));
        System.out.println(System.currentTimeMillis() - l);
    }

}
