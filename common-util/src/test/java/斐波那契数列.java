public class 斐波那契数列 {

    public static void main(String[] args) {
        fibonacci(42);
    }

    public static void fibonacci(int n) {
        int preNo = 0;
        int preNoTwo = 1;
        System.out.print(1 + " ");
        for (int i = 1; i <= n; i++) {
            int total = preNo + preNoTwo;
            System.out.print(total + " ");
            preNo = preNoTwo;
            preNoTwo = total;
        }
    }
}
