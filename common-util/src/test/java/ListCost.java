import java.util.ArrayList;
import java.util.List;

public class ListCost {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<Integer>() {{
            add(20);
            add(30);
            add(400);
        }};

        cost(list, 66);
    }

    private static Integer cost(List<Integer> list, Integer cost) {
        Integer next = cost;
        for (Integer it : list) {
            if (next > 0) {
                if (next > it) {
                    next = next - it;
                    it = 0;
                } else {
                    next = it - next;
                    it = next;
                }
            } else {
                it = it - cost;
            }
            System.out.println(it);
        }
        return 0;
    }
}
