public class BinaryUtil {

    public static String toBinary(int x) {
        int length = 32 - leadingZerox(x);
        int i = -1;
        StringBuilder sb = new StringBuilder();
        while (i++ < length && x > 0) {
            sb.append(x & 1);
            x >>= 1;
        }
        return sb.reverse().toString();
    }

    private static int leadingZerox(int x) {
        int z = 0;
        if (x >>> 16 == 0) {
            z += 16;
            x <<= 16;
        }
        if (x >>> 24 == 0) {
            z += 8;
            x <<= 8;
        }
        if (x >>> 28 == 0) {
            z += 4;
            x <<= 2;
        }
        if (x >>> 30 == 0) {
            z += 2;
            x <<= 2;
        }
        z -= x >>> 31;
        return 32 - z;
    }

    public static String toBinary(long x) {
        return "";
    }

    public static void main(String[] args) {
        System.out.println(Long.toBinaryString(0x7fffffff));
    }
}
