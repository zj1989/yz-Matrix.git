package java;

public class ABC轮流打印算法 {

    public static void main(String[] args) throws InterruptedException {
        Object a = "a";
        Object b = "b";
        Object c = "c";
        Thread ta = new Thread(new ABC轮流打印线程(b, a, "a", p -> System.out.println(p)));
        ta.start();
        Thread tb = new Thread(new ABC轮流打印线程(c, b, "b", p -> System.out.println(p)));
        tb.start();
        Thread tc = new Thread(new ABC轮流打印线程(a, c, "c", p -> System.out.println(p)));
        tc.start();
    }
}
