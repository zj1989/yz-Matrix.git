import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class 打印集合内不同元素 {
    public static void main(String[] args) {
        List<Integer> logs = Arrays.asList(1, 8, 2, 5, 6, 7);
        Collections.sort(logs);
        List<Integer> trans = Arrays.asList(1, 8, 2, 9, 2, 3, 4, 5, 6, 7);
        Collections.sort(trans);

        int x = 0, y = 0;
        for (; x < logs.size() && y < trans.size(); ) {
            if (logs.get(x).equals(trans.get(y))) {
                x++;
                y++;
                continue;
            }
            if (logs.get(x) < trans.get(y)) {
                x++;
            } else {
                System.out.println(trans.get(y));
                y++;
            }
        }
        if (y < trans.size()) {
            for (int a = y; a < trans.size(); a++) {
                System.out.println(trans.get(a));
            }
        }
    }
}
