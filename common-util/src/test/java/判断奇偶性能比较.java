public class 判断奇偶性能比较 {

    public static void main(String[] args) {
        long count = 6000000000L;
        long ct = 0;
        long l = System.currentTimeMillis();
        for (long i = 0; i < count; i++) {
            if ((i++ % 2) == 1) {
                ct++;
                System.out.println(ct);
            }
        }
        System.out.println(System.currentTimeMillis() - l);

        long ct1 = 0;
        long l2 = System.currentTimeMillis();
        for (long i = 0; i < count; i++) {
            if ((i++ & 1) == 1) {
                ct1++;
                System.out.println(ct1);
            }
        }
        System.out.println(System.currentTimeMillis() - l2);
    }
}
