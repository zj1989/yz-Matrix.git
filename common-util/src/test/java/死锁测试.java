public class 死锁测试 {
    public static void main(String[] args) {
        String a = "a";
        String b = "b";
        Thread t1 = new Thread(new 死锁测试线程(a, b));
        Thread t2 = new Thread(new 死锁测试线程(b, a));
        t1.start();
        t2.start();
    }
}
