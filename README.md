# yz-Matrix

#### 介绍
发挥自己的想象，制作更多的轮子，让开发更简单。

#### 软件架构
**模块：basic-impl-fun**<br/>
功能：针对springboot提取基础增删改查能力
中央仓库地址：
````java
<dependency>
  <groupId>com.xwsos</groupId>
  <artifactId>basic-impl-fun</artifactId>
  <version>搜索最新版本</version>
</dependency>
````
功能：
- 自动生成表结构
- 实现mysql与MongoDB的增加、删除、查询、更新操作

#### 使用
1. 配置（mysql与mongo的按需配置）
    - spring.datasource(dirver-class、url、username、password、type)
    - spring.jpa(show-sql、hibernate.ddl-auto、database-platform、open-in-view:false)
    - spring.mongodb(database、host、port、auto-index-creation、repositories.type)

2. 公共对象，实现后即可生成对应表、对象
    - 实体对象
        - com.lz.model.entity.CommonEntity 基本mysql对象，继承此类，提供基础字段，请按必填要求填充内容
        - com.lz.model.document.BaseDoc 基本mongo对象，继承此类，提供基础字段
    - 条件对象
        - com.lz.model.vo.CommonBaseCondition 继承此类，实现mysql查询条件的接收
        - com.lz.model.vo.DocumentCondition 无需继承此对象，此对象接收mongo对象的查询 

3. 创建Controller
    - com.lz.controller.BaseController 继承此类，提供相应的泛型，即完成了mysql的基础增删改查功能
    - com.lz.controller.BaseMongoController 继承此类，提供相应的泛型，即完成了mongo的基础增删改查功能
    
#### 实例
- 配置
```java
spring:
  datasource:
    driver-class: com.mysql.jdbc.Driver
    url: jdbc:mysql://192.168.1.6:3306/mnsq?useSSL=false&useUnicode=true&characterEncoding=utf-8
    username: root
    password: 9999
    type: com.alibaba.druid.pool.DruidDataSource
  jpa:
    show-sql: true
    hibernate:
      ddl-auto: update
    database-platform: org.hibernate.dialect.MySQL5InnoDBDialect
    open-in-view: false
  data:
    mongodb:
      database: test
      host: 192.168.1.6
      port: 27017
      auto-index-creation: false
      repositories:
        type: none
```
- controller

```java
// 访问地址：
// /api/xxxx/list
// /api/xxxx/detail
// /api/xxxx/page、/api/xxxx/update、/api/xxxx/add、/api/xxxx/del、/api/xxxx/batch/del
// 增删改需要提供用户id，填充：com.lz.conf.ThreadLocalUserId.CURRENT_USER_ID，可使用切面实现填充
@RequestMapping("/api/xxxx")
public class DynamicController extends BaseController<UserDynamic, BaseService, DynamicCondition>{}

BaseService：只提供基础功能
```

- entity
```java
@Data
@Accessors(chain = true)
@Entity
@Table(name = "user_dynamic")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDynamic extends CommonEntity {}
```

- condition
```java
@Data
@Accessors(chain = true)
public class DynamicCondition extends CommonBaseCondition<UserDynamic> {}
```

- MongoDB，与mysql类型，单功能更丰富（类似graphQL）
